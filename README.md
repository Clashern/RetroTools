**RetroTools**

RetroTools est un ensemble de scripts écrits en batch et powershell afin de faciliter la gestion des roms pour les différents émulateurs et frontends.

A ce jour, RetroTools est composé de :

- **M3U_Generator.bat** : un outil permet de créer des fichiers M3U à partir d'un répertoire comprenant des jeux multidisques ou non, supportant les archives aux formats ZIP, 7Z et RAR.
> Attention: M3U_Generator ne prend pas en charge les fichiers situés dans des sous-dossiers, ni les fichiers compressés comprenant des dossiers.

<details><summary>English section</summary>
RetroTools is a set of scripts written in batch and powershell in order to facilitate the management of roms for the various emulators and frontends.

To date, RetroTools is composed of :

- **M3U_Generator.bat** : a tool to create M3U files from a directory including multidisks games or not, supporting archives in ZIP, 7Z and RAR formats.
> Warning: M3U_Generator does not support files located in subfolders, nor compressed files with folders.
</details>

<details><summary>Sezione italiana</summary>
RetroTools è un insieme di script scritti in batch e powershell per facilitare la gestione delle rom per diversi emulatori e frontend.

Ad oggi, RetroTools è composto da :

- **M3U_Generator.bat**: uno strumento per creare file M3U da una directory che include multi-disco o meno, supportando archivi in formato ZIP, 7Z e RAR.
> Attenzione: M3U_Generator non supporta i file in sottocartelle, né i file compressi con cartelle.
</details>

<details><summary>Deutsche Sektion</summary>
RetroTools ist eine Sammlung von Skripten, die in Batch und Powershell geschrieben wurden, um die Verwaltung von Roms für verschiedene Emulatoren und Frontends zu erleichtern.

Bisher besteht RetroTools aus :

- **M3U_Generator.bat**: Ein Tool zum Erstellen von M3U-Dateien aus einem Verzeichnis, das Spiele mit oder ohne Multidisk enthält, und das Archive in den Formaten ZIP, 7Z und RAR unterstützt.
> Achtung: M3U_Generator unterstützt keine Dateien in Unterordnern oder komprimierte Dateien, die Ordner enthalten.
</details>

<details><summary>Sección española</summary>
RetroTools es un conjunto de scripts escritos en batch y powershell para facilitar la gestión de roms para diferentes emuladores y frontends.

Hasta la fecha, RetroTools se compone de :

- **M3U_Generator.bat**: una herramienta para crear archivos M3U a partir de un directorio, incluso multidisco o no, que soporta archivos ZIP, 7Z y RAR.
> Atención: M3U_Generator no admite archivos en subcarpetas, ni archivos comprimidos con carpetas.
</details>
