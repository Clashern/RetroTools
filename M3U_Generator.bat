@echo off
cls
mode con cols=100 lines=50
color 06
set releaseDate=22.11.2021
title M3U Generator ver. %releaseDate%

echo ====================================================================================================
echo =                                                                                                  =
echo =                                   M3U GENERATOR - by Clashern                                    =
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo Please wait...

::Dictionnaire
set lang=auto

::Création du dictionnaire
	::Changement du code en Unicode/1252 (prise en charge des accents)
		chcp 1252 >nul

	::Acquisition du langage de l'OS
		if not "%lang%"=="auto" goto selectDictionary
		for /f "tokens=3" %%a in ('reg Query "HKCU\Control Panel\Desktop\MuiCached" /V MachinePreferredUILanguages ^| find "MachinePreferredUILanguages"') do set language=%%a
		set language=%language:~0,-3%

	:selectDictionary
	::Sélection du dictionnaire
		for %%a in (es de fr en it) do (
			if "%%a"=="%lang%" set language=%lang%
		)
				if "%language%"=="es" goto es
				if "%language%"=="de" goto de
				if "%language%"=="fr" goto fr
				if "%language%"=="it" goto it
				goto *
		
		:es
			set "dict=Descarga e instalación;Limpieza de archivos descargados;¿Dónde se encuentran los artículos?;En la siguiente ventana, especifique el directorio;Elija un directorio;Ha elegido el directorio;¿Es esto correcto?;Sí;No;Salir;(por defecto);¿Tu elección?;¿Deben comprimirse los archivos finales? En caso afirmativo, ¿en qué formato?;Sin compresión;Volver;¿Debo crear una copia de seguridad de los archivos?;Resumen;Directorio seleccionado;Compresión de archivos;Sin respaldo;Copia de seguridad de los archivos;Copia de seguridad en curso;Por favor, espere;Juego en curso;¿Desea sustituir los archivos de origen?;Mudanza en curso;Trabajo realizado;Pulse una tecla para salir;falló;Archivos procesados;Archivos creados" & goto createDict
		:de
			set "dict=Herunterladen und Installieren von;Bereinigung der heruntergeladenen Dateien;Wo befinden sich die Elemente?;Im folgenden Fenster geben Sie bitte das Verzeichnis an;Bitte wählen Sie ein Verzeichnis aus;Sie haben das Verzeichnis ausgewählt;Ist es korrekt?;Ja;Nein;Beenden;(standardmäßig);Wie haben Sie sich entschieden?;Sollen die endgültigen Dateien komprimiert werden? Wenn ja, in welchem Format?;Keine Komprimierung;Zurück;Soll eine Sicherungskopie der Dateien erstellt werden?;Zusammenfassung;Gewähltes Verzeichnis;Komprimierung der Dateien;Keine Sicherung;Sicherung der Dateien;Datensicherung wird durchgeführt;Bitte warten Sie;Spiel läuft;Möchten Sie die Quelldateien ersetzen?;Verschieben wird durchgeführt;Arbeit abgeschlossen;Drücken Sie eine Taste, um zu beenden;gescheitert;Verarbeitete Dateien;Erstellte Dateien" & goto createDict
		:fr
			set "dict=Téléchargement et installation de;Nettoyage des fichiers téléchargés;Où se trouvent les éléments ?;Dans la fenêtre suivante, veuillez spécifier le répertoire;Merci de choisir un répertoire;Vous avez choisi le répertoire;Est-ce correct ?;Oui;Non;Quitter;(par défaut);Votre choix ?;Faut-il compresser les fichiers finaux ? Si oui, quel format ?;Pas de compression;Retour;Faut-il créer une sauvegarde des fichiers ?;Récapitulatif;Répertoire choisi;Compression des fichiers;Pas de sauvegarde;Sauvegarde des fichiers;Sauvegarde en cours;Merci de patienter;Jeu en cours;Souhaitez-vous remplacer les fichiers sources ?;Déplacement en cours;Travail terminé;Appuyez sur une touche pour quitter;échoué;Fichiers traités;Fichiers créés" & goto createDict
		:it
			set "dict=Scaricare e installare;Pulizia dei file scaricati;Dove si trovano gli oggetti?;Nella seguente finestra, specificare la directory;Per favore, scegli una directory;Avete scelto la directory;È corretto?;Sì;No;Uscita;(predefinito);La tua scelta?;I file finali devono essere compressi? Se sì, quale formato?;Nessuna compressione;Indietro;Devo creare un backup dei file?;Riassunto;Elenco selezionato;Compressione dei file;Nessun backup;Backup dei file;Backup in corso;Si prega di attendere;Gioco in corso;Volete sostituire i file di origine?;Spostamento in corso;Lavoro completato;Premere un tasto per uscire;fallito;File elaborati;File creati" & goto createDict
		:*
			if not "%language%"=="en" echo Can't get OS's language. English will be used.
			set "dict=Download and installation of;Cleaning up downloaded files;Where are the items located?;In the following window, please specify the directory;Please choose a directory;You have chosen the directory;Is this correct?;Yes;No;Exit;(by default);Your choice?;Should the final files be compressed? If yes, which format?;No compression;Back;Should I create a backup of the files?;Summary;Selected directory;File compression;No backup;Backup of files;Backup in progress;Please wait;Game in progress;Would you like to replace the source files?;Moving in progress;Work completed;Press any key to exit;failed;Processed files;Files created"
		
		:createDict
		set /a count=0
		call :parseDict "%dict%"

			:parseDict
			set /a count+=1
			for /f "tokens=1* delims=;" %%i in ("%~1") do (
				set dict[%count%]=%%i
				call :parseDict "%%j"
			)

:checkvar
::Attibution des outils nécessaires à des variables.
set launchdir=%cd%
set toolsdir=%launchdir%\ressources
set zipdir=%toolsdir%\7za
set wgetdir=%toolsdir%\wget
set tmpdir=%toolsdir%\tmp

::Vérification des outils nécessaires
if not exist %toolsdir% mkdir %toolsdir%
if not exist %zipdir% mkdir %zipdir%
if not exist %wgetdir% mkdir %wgetdir%

if exist %toolsdir%\7za.zip (
	move /y "%toolsdir%\7za.zip" "%zipdir%\7za.zip">nul
	powershell -command "Expand-Archive -Force -LiteralPath %zipdir%\7za.zip -DestinationPath %zipdir%"
	ping 127.0.0.1 -n 2>nul
	del %zipdir%\7za.zip>nul
)

if exist %toolsdir%\wget.zip (
	move /y "%toolsdir%\wget.zip" "%wgetdir%\wget.zip">nul
	%zipdir%\7za.exe x "%wgetdir%\wget.zip" -o"%wgetdir%" -aoa>nul
	ping 127.0.0.1 -n 2>nul
	del %wgetdir%\wget.zip>nul
)

::Nettoyage du dossier temporaire
if exist %tmpdir% rmdir %tmpdir% /s /q>nul
mkdir %tmpdir%>nul

:check7z
::Vérification que 7z est installé pour utilisation avec le fichier bat.
if exist %zipdir%\7za.exe goto checkwget

:install7z
::Installation de 7z.
cls
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[1]% 7ZA.
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
::Téléchargement de 7za et décompression via Powershell. (Les futures décompressions se feront via 7za, plus rapide et plus propre.)
powershell -command "(New-Object Net.WebClient).DownloadFile('https://gitlab.com/Clashern/RetroTools/-/raw/main/ressources/7za.zip','%zipdir%\7za.zip')
powershell -command "Expand-Archive -Force -LiteralPath %zipdir%\7za.zip -DestinationPath %zipdir%"
cls
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[2]%.
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
::Attente que le fichier zip soit complètement fermé.
ping 127.0.0.1 -n 2 >nul
::Suppression du zip téléchargé.
del %zipdir%\7za.zip
cls
if not exist %zipdir%\7za.exe goto 7zerror
goto checkwget

:checkwget
::Vérification que wget est installé pour utilisation avec le fichier bat.
if exist %wgetdir%\wget.exe goto menu

:installwget
::Installation de wget.
cls
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[1]% Wget.
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
::Téléchargement de wget via Powershell et décompression via 7za. (Les futurs téléchargements se feront via wget, plus rapide et plus propre.)
powershell -command "(New-Object Net.WebClient).DownloadFile('https://gitlab.com/Clashern/RetroTools/-/raw/main/ressources/wget.zip','%wgetdir%\wget.zip')
ping 127.0.0.1 -n 3 > nul
%zipdir%\7za.exe x "%wgetdir%\wget.zip" -o"%wgetdir%" -aoa > nul
cls
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[2]%.
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
::Attente que le fichier zip soit complètement fermé.
ping 127.0.0.1 -n 2 >nul
::Suppression du zip téléchargé.
del %wgetdir%\wget.zip
cls
if not exist %wgetdir%\wget.exe goto wgeterror
goto menu

::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------

:menu
::Choix du répertoire de travail.
cls
echo ====================================================================================================
echo =                                                                                                  =
echo =          %dict[3]%
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo %dict[4]%.
echo.
pause
set "psCommand="(new-object -COM 'Shell.Application')^
.BrowseForFolder(0,'%dict[5]%.',0,0).self.path""
for /f "usebackq delims=" %%f in (`powershell %psCommand%`) do set "folder=%%f"

cls
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[6]% %folder%
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
echo ====================================================================================================
echo =                                                                                                  =
echo =          %dict[7]%
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo ( 1 ) - %dict[8]%
echo.
echo ( 2 ) - %dict[9]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
echo ( Q ) - %dict[10]% %dict[11]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
set/p dirok="%dict[12]% " || set dirok=Q
if %dirok%==Q exit
if %dirok%==q exit
if %dirok%==1 goto zipchoice
if %dirok%==1 goto zipchoice
goto menu

:zipchoice
::Choix de la compression finale.
cls
echo ====================================================================================================
echo =                                                                                                  =
echo =          %dict[13]%
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo ( 0 ) - %dict[14]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
echo ( 1 ) - 7z
echo.
echo ( 2 ) - Zip
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
echo ( R ) - %dict[15]% %dict[11]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
echo ( q ) - %dict[10]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
set/p zchoice="%dict[12]% " || set zchoice=R
if %zchoice%==0 goto savechoice
if %zchoice%==1 set "ziparg=a -t7z -m0=lzma2 -mx=9 -aoa -r" & set "zipext=7z" & goto savechoice
if %zchoice%==2 set "ziparg=a -tzip -aoa -r" & set "zipext=zip" & goto savechoice
if %zchoice%==R goto menu
if %zchoice%==r goto menu
if %zchoice%==q exit
if %zchoice%==Q exit
goto zipchoice

:savechoice
::Demande si travail dans répertoire local ou sauvegarde dans répertoire temporaire.
cls
echo ====================================================================================================
echo =                                                                                                  =
echo =          %dict[16]%
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo ( 0 ) - %dict[8]%
echo.
echo ( 1 ) - %dict[9]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
echo ( R ) - %dict[15]% %dict[11]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
echo ( q ) - %dict[10]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
set/p schoice="%dict[12]% " || set schoice=R
if %schoice%==0 goto startjob
if %schoice%==1 goto startjob
if %schoice%==R goto zipchoice
if %schoice%==r goto zipchoice
if %schoice%==q exit
if %schoice%==Q exit
goto savechoice

:recap
::Récapitulatif avant travail.
cls
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[17]%.
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
echo.
echo %dict[18]%: %folder%
echo.
if %zchoice%==0 echo %dict[14]%
if not %zchoice%==0 echo %dict[19]%: %zipext%
echo.
if %schoice%==1 (
	echo %dict[20]%
	) else (
	echo %dict[21]%)
echo.
echo ====================================================================================================
echo =                                                                                                  =
echo =          %dict[7]%
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo ( 1 ) - %dict[8]%
echo.
echo ( 2 ) - %dict[9]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
echo ( R ) - %dict[15]% %dict[11]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
set/p recapok="%dict[12]% " || set recapok=R
if %dirok%==R goto savechoice
if %dirok%==r goto savechoice
if %dirok%==1 goto startjob
if %dirok%==2 goto menu
goto recap

::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------

:startjob
::Nettoyage du dossier temporaire
if exist %tmpdir% rmdir %tmpdir% /s /q>nul
mkdir %tmpdir%>nul

if %schoice%==1 cd %folder% & goto startjob1

::Copie du répertoire choisi vers le répertoire temporaire.
cls
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[22]%. %dict[23]%.
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
for %%i in (%folder%\*) do (
	copy /y "%%i" "%tmpdir%">nul
)

cd %tmpdir%
if exist "%tmpdir%\*.txt" del /q "%tmpdir%\*.txt">nul
if exist "%tmpdir%\*.xml" del /q "%tmpdir%\*.xml">nul

:startjob1
::Création des répertoires temporaires correspondants à chacun des jeux contenant un numéro de disque 1/X entre parenthèses.
cls
set filescnt=1
for /f "delims=" %%a in ('dir /b /o:gen') do (
	set /a filescnt+=1
	echo ----------------------------------------------------------------------------------------------------
	echo -                                                                                                  -
	echo -          %dict[24]%: %%~na
	echo -                                                                                                  -
	for /f "delims=(" %%i in ("%%a") do (
		if not exist "%%i\." mkdir "%%i">nul
		move /y "%%a*" "%%i\.">nul
	)
)

::Création des répertoires temporaires correspondants à chacun des jeux NE contenant PAS un numéro de disque 1/X entre parenthèses.
for %%i in (.\*.*) do (
	::Exclusion du fichier bat.
	if not "%%i"==".\M3U_Generator.bat" (
		::Exclusion des fichiers txt.
		if not "%%~xi"==".txt" (
			set /a filescnt+=1
			echo ----------------------------------------------------------------------------------------------------
			echo -                                                                                                  -
			echo -          %dict[24]%: %%~ni
			echo -                                                                                                  -
			if not exist "%%~ni\." mkdir "%%~ni">nul
			move /y "%%i" "%%~ni">nul
		)
	)
)

cls

::Décompression des archives, puis création du fichier M3U correspondant et enfin déplacement.
set totalcnt=0
for /f "delims=" %%a in ('dir /ad /b /o:gen') do (
	set /a totalcnt+=1
	echo ----------------------------------------------------------------------------------------------------
	echo -                                                                                                  -
	echo -          %dict[24]%: %%~na
	echo -                                                                                                  -
	for %%i in ("%%a\*.zip", "%%a\*.7z", "%%a\*.rar") do (
		::Décompression des archives.
		%zipdir%\7za.exe -y x "%%i" -o"%%a" -aoa>nul
	)
		
	::Suppression des archives.
	if exist "%%a\*.zip" del/q "%%a\*.zip">nul
	if exist "%%a\*.7z" del/q "%%a\*.7z">nul
	if exist "%%a\*.rar" del/q "%%a\*.rar">nul
	
	if not %zchoice%==0 (
		for /r %%i in ("%%a\*.*") do (
			if not "%%~xi"==".%zipext%" %zipdir%\7za.exe %ziparg% "%%a\%%~ni.%zipext%" "%%i">nul
			if not "%%~xi"==".%zipext%" del/q "%%i">nul
		)
	)
	::Création du fichier M3U correspondant à chaque réperoire.
	dir "%%a" /b>"%%a.m3u"
	::Déplacement des fichiers dans le répertoire racine.
	move /y "%%a\*" ".\.">nul
	::Suppression des répertoires temporaires.
	rmdir /q "%%a">nul
)

if %schoice%==1 goto endjob

:finalmenu
cls
echo ====================================================================================================
echo =                                                                                                  =
echo =          %dict[25]%
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo ( 1 ) - %dict[8]%
echo.
echo ( 2 ) - %dict[9]% %dict[11]%
echo.
echo ----------------------------------------------------------------------------------------------------
echo.
set/p finaljob="%dict[12]% " || set finaljob=2
if %finaljob%==1 set finaldir=_save
if %finaljob%==2 set finaldir=_converted

cls
if not exist "%folder%\%finaldir%" mkdir "%folder%\%finaldir%">nul
echo ----------------------------------------------------------------------------------------------------
echo -                                                                                                  -
echo -          %dict[26]%. %dict[23]%.
echo -                                                                                                  -
echo ----------------------------------------------------------------------------------------------------
if %finaljob%==1 (
	move /y "%folder%\*" "%folder%\%finaldir%">nul
	move /y "%tmpdir%\*" "%folder%">nul
)
if %finaljob%==2 (
	move /y "%tmpdir%\*" "%folder%\%finaldir%">nul
)
if not exist "%folder%\%finaldir%\*.txt" goto endjob
for /f "delims=" %%i in ("%folder%\%finaldir%\*.txt") do (
	move /y "%%i" "%folder%">nul
)

:endjob
cls
echo ====================================================================================================
echo =                                                                                                  =
echo =          %dict[27]%
echo =                                                                                                  =
echo ====================================================================================================
echo.
echo %dict[30]% = %filescnt%
echo.
echo %dict[31]% = %totalcnt%
echo.
echo %dict[28]%.
echo.
pause >nul
exit

::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------
::----------------------------------------------------------------------------------------------------

:7zerror
::Affichage erreur si le téléchargement de 7z a échoué.
cls
echo ****************************************************************************************************
echo *                                                                                                  *
echo *          %dict[1]% 7ZA %dict[29]%.
echo *                                                                                                  *
echo ****************************************************************************************************
echo.
echo %dict[28]%.
pause >nul
exit

:wgeterror
::Affichage erreur si le téléchargement de wget a échoué.
cls
echo ****************************************************************************************************
echo *                                                                                                  *
echo *         %dict[1]% Wget %dict[29]%.
echo *                                                                                                  *
echo ****************************************************************************************************
echo.
echo %dict[28]%.
pause >nul
exit
